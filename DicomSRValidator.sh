#!/bin/sh

PIXELMEDDIR=.

#java \
#	-Djavax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl \
#	-Xmx2g -Xms512m -XX:-UseGCOverheadLimit \
#	-cp "${PIXELMEDDIR}/pixelmed.jar:${PIXELMEDDIR}/lib/additional/slf4j-api-1.7.13.jar:${PIXELMEDDIR}/lib/additional/slf4j-simple-1.7.13.jar:${PIXELMEDDIR}/lib/additional/excalibur-bzip2-1.0.jar:${PIXELMEDDIR}/lib/additional/commons-codec-1.3.jar:/opt/local/share/java/saxon9he.jar" \
#	com.pixelmed.validate.DicomSRValidator $*

# override new default limits added in 1.8.0_331 for xalan processor (https://www.oracle.com/java/technologies/javase/8u331-relnotes.html)
java \
    -Djdk.xml.xpathExprGrpLimit=0 \
    -Djdk.xml.xpathExprOpLimit=0 \
    -Djdk.xml.xpathTotalOpLimit=0 \
	-Xmx2g -Xms512m -XX:-UseGCOverheadLimit \
	-cp "${PIXELMEDDIR}/pixelmed.jar:${PIXELMEDDIR}/lib/additional/slf4j-api-1.7.13.jar:${PIXELMEDDIR}/lib/additional/slf4j-simple-1.7.13.jar:${PIXELMEDDIR}/lib/additional/excalibur-bzip2-1.0.jar:${PIXELMEDDIR}/lib/additional/commons-codec-1.3.jar" \
	com.pixelmed.validate.DicomSRValidator $*
