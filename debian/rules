#!/usr/bin/make -f
#export DH_VERBOSE=1

export JAVA_HOME=/usr/lib/jvm/default-java

include /usr/share/dpkg/default.mk

%:
	dh $@ --with javahelper

override_dh_auto_build:
	$(MAKE) JAVAVERSIONTARGET=8 all
	$(MAKE) javadoc

# See #568897
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test:
	$(MAKE) -C com/pixelmed/test alltests || true
endif

override_dh_auto_install:
	cp sample.com.pixelmed.display.DicomImageViewer.properties .com.pixelmed.display.DicomImageViewer.properties
	# executable-not-elf-or-script ./etc/pixelmed/.com.pixelmed.display.DicomImageViewer.properties
	chmod -x .com.pixelmed.display.DicomImageViewer.properties
	dh_auto_install

override_dh_clean:
	dh_clean com/pixelmed/test/tmp/TestCleanerReceiveAndClean/1.3.6.1.4.1.5962.99.1.4198289672.2040812585.1361407987975.4.0
	dh_clean com/pixelmed/validate/die/verwandlung/die_verwandlung*.class
	dh_clean BUILDDATE
	dh_clean manifest.txt
	dh_clean .com.pixelmed.display.DicomImageViewer.properties
	# doc package:
	rm -rf docs

VER_FULL = $(DEB_VERSION_UPSTREAM)

debian/%.1: debian/%.1.in
	help2man --no-discard-stderr --include=$< --output=$@ --version-string=$(VER_FULL) --no-info `basename $@ .1`

pixelmed_manpages: debian/DicomSRValidator.1 debian/ImageToDicom.1 debian/NIfTI1ToDicom.1 debian/NRRDToDicom.1 debian/PDFToDicomImage.1 debian/StructuredReport.1 debian/VerificationSOPClassSCU.1 debian/TIFFToDicom.1
	echo "all manpages done"

debian/%.desktop: webstart/%.jnlp debian/jnlp2desktop.xsl
	# create .desktop file:
	xsltproc --stringparam pixelmed_version $(DEB_VERSION_UPSTREAM) -o $@ debian/jnlp2desktop.xsl $<

debian/%: webstart/%.jnlp debian/jnlp2script.xsl debian/%.desktop
	# script wrapper:
	xsltproc --stringparam pixelmed_version $(DEB_VERSION_UPSTREAM) -o $@ debian/jnlp2script.xsl $<
	chmod +x $@

pixelmed_webapps: debian/DicomImageBlackout debian/MediaImporter debian/WatchFolderAndSend debian/DicomImageViewer debian/ConvertAmicasJPEG2000FilesetToDicom debian/DicomCleaner debian/DoseUtility
	echo "all shell script wrappers done"
